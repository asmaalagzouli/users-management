import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../classes/user';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private apiUrl = environment.apiPrefix;

  constructor(
    private http: HttpClient
  ) { }

  getUsers({ page, perPage }: { page: number; perPage: number; }) {
    return this.http.get(`${this.apiUrl}/users?page=${page}&per_page=${perPage}`);
  }

  delete(userId: number) {
    return fetch(`${this.apiUrl}/users/${userId}`, {
      method: 'DELETE'
    })
      .then(response => {
        if (response.ok) {
          console.log('User deleted successfully');
        } else {
          console.error('Failed to delete user');
        }
      })
      .catch(error => {
        console.error('Error occurred while deleting user:', error);
      });
  }
}
