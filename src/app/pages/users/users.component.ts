import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserService } from 'src/app/services/user.service';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  private unsubscribe: Subject<void> = new Subject();

  users: any[] = [];
  currentPage = 1;
  totalPages: number[] = [];

  constructor(
    private http: HttpClient,
    private userService: UserService,
  ) {
    
  }

  ngOnInit(): void {
    this.getUsers(this.currentPage);
  }

  getUsers(page: number) {
    // this.userService.getUsers({ page, perPage: 6 })
    // .pipe(takeUntil(this.unsubscribe))
    // .subscribe((response: any) => {
    //   console.log(response);
      
    //   this.users = response;
    //   this.totalPages = Array.from({ length: response.total_pages }, (_, index) => index + 1);
    //   console.log(this.totalPages);
    // });

    this.http.get(`https://reqres.in/api/users?page=${page}`)
      .subscribe((response: any) => {
        this.users = response.data;
        this.totalPages = Array.from({ length: response.total_pages }, (_, index) => index + 1);
      });
  }

  destroy (id: number) {
    this.userService.delete(id).then(() => {
      console.log('user has been deleted !');
    })
    .catch(() => {
      console.log('user hasnt been deleted !')

    })
  }

  getPage(page: number) {
    if (page !== this.currentPage) {
      this.currentPage = page;
      this.getUsers(this.currentPage);
    }
  }

  previousPage() {
    if (this.currentPage > 1) {
      this.currentPage--;
      this.getUsers(this.currentPage);
    }
  }

  nextPage() {
    if (this.currentPage < this.totalPages.length) {
      this.currentPage++;
      this.getUsers(this.currentPage);
    }
  }
}
