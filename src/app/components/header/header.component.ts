import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LanguageService } from 'src/app/services/language.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  languages: any = [
    {language: "fr", flag: "/assets/imgs/frensh.png"},
    {language: "en", flag: "/assets/imgs/uk.jpg"}
  ];
  defaultLang: string;
  
  constructor(
    private translateService: TranslateService, 
    private languageService: LanguageService
  ) {
    this.defaultLang = this.translateService.defaultLang;
  }

  get selectedLanguage(): string {
    return this.languageService.getSelectedLanguage();
  }

  set selectedLanguage(language: string) {
    this.languageService.setSelectedLanguage(language);
    this.switchLanguage(language);
  }

  switchLanguage(lang: string) {
    this.translateService.use(lang);
    this.translateService.currentLang = lang;
  }
}
